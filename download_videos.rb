require 'rubygems'
require 'capybara'
require 'capybara/dsl'
require 'pry'

require 'selenium/webdriver'
Capybara.run_server = false
Capybara.default_driver = :selenium

Capybara.app_host = 'http://www.ml-class.org'
class ML
	include Capybara::DSL
	def download(user_name,password)
		visit("/course/auth/welcome")
		click_link("login_box_link")
		within(:css,"#fancybox-content") do
		  fill_in 'email_address', :with => user_name
		  fill_in 'password', :with => password
		  click_button 'Login'
		end
		visit("/course/video/list?mode=download")
		all('a.default_link').each do |vid|
			if vid[:href] =~ /download\?video_id/
				puts vid[:text]
				visit vid[:href]
			#	puts "Waiting"
			#  sleep(20)
			#	puts "waited"
			 # page.driver.browser.switch_to.alert.accept
			#  click_link("OK") #finally download the video
			end
		end
	end
end

user_name = ARGV[0]
password = ARGV[1]
lectures = ML.new
lectures.download(user_name,password)




